# News App

## News App build in Flutter.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


# News App UI Design 

## home page
![Screenshot_from_2020-10-05_10-52-57](/uploads/2c3db3d0b38692350b80b3d26852805f/Screenshot_from_2020-10-05_10-52-57.png)

## single new list
![Screenshot_from_2020-10-05_10-53-29](/uploads/211b8f062d060511273048043244ad74/Screenshot_from_2020-10-05_10-53-29.png)

## single category news list
![Screenshot_from_2020-10-05_10-55-14](/uploads/38a4934d58bb79f3ede63bb7f23a84fe/Screenshot_from_2020-10-05_10-55-14.png)





