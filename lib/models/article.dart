class ArticleModel {

  String author;
  String title;
  String description;
  String url;
  String urlImage;
  String content;
  DateTime publishedAt;



  ArticleModel({this.author,this.title,this.description,this.url,this.urlImage,this.content,this.publishedAt});



}