import 'package:flutter/material.dart';
import 'package:news_app/helper/categoryNews.dart';
import 'package:news_app/models/category_model.dart';

class CategoryNews extends StatefulWidget {
  final String category;

  CategoryNews({this.category});

  @override
  _CategoryNewsState createState() => _CategoryNewsState();
}

class _CategoryNewsState extends State<CategoryNews> {

  List<CategoryModel> categoryModel = new List<CategoryModel>();

  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    this.getCategoryNews();
  }
   /// fetch news category list from api
  getCategoryNews() async {

    setState(() {
      _isLoading = true;
    });

    CategoryArticles categoryArticles = CategoryArticles();

    await categoryArticles.getCategoryArticles(widget.category);

    categoryModel = categoryArticles.categories;

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.black26,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Flutter',
              style: TextStyle(color: Colors.black87),
            ),
            Text('News',
                style: TextStyle(
                    color: Colors.blue,
                    letterSpacing: 1.0,
                    fontWeight: FontWeight.w600))
          ],
        ),
      ),
      body: _isLoading
          ? Center(
              child: Container(
                child: CircularProgressIndicator(),
              ),
            )
          : SingleChildScrollView(
              child: Container(
                child: Column(
                  children: [
                    // news category list
                    Container(
                      padding: EdgeInsets.only(top: 16),
                      child: ListView.builder(
                          itemCount: 4,
                          shrinkWrap: true,
                          physics: ScrollPhysics(),
                          itemBuilder: (context, index) {
                            return NewsCategoryList(
                              author: categoryModel[index].categoryName,
                              image: categoryModel[index].imageUrl,
                            );
                          }),
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}

/* 2 widgets*/
class NewsCategoryList extends StatelessWidget {

  final author, image;
  NewsCategoryList({@required this.author, @required this.image});
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(bottom: 16),
      child: Column(
        children: [
          ClipRRect(
              borderRadius: BorderRadius.circular(13.0),
              child: Image.network(image)),
          SizedBox(height: 20.0),
          Text(
            author,
            style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w700),
          ),
        ],
      ),
    );
  }
}
