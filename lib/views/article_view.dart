import 'dart:async';

import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ArticlesView extends StatefulWidget {
  final String linkURL;

  ArticlesView({this.linkURL});

  @override
  _ArticlesViewState createState() => _ArticlesViewState();
}

class _ArticlesViewState extends State<ArticlesView> {
  final Completer<WebViewController> _completer =
      Completer<WebViewController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          elevation: 0,
          backgroundColor: Colors.black26,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Flutter',
                style: TextStyle(color: Colors.black87),
              ),
              Text('News',
                  style: TextStyle(
                      color: Colors.blue,
                      letterSpacing: 1.0,
                      fontWeight: FontWeight.w600))
            ],
          ),
        ),
        body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: WebView(
              initialUrl: widget.linkURL,
              onWebViewCreated: (WebViewController webViewController) {
                _completer.complete(webViewController);
              },
            )
          )
    );
  }
}
