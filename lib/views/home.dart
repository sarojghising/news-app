import 'package:flutter/material.dart';
import 'package:news_app/helper/data.dart';
import 'package:news_app/helper/news.dart';
import 'package:news_app/models/article.dart';
import 'package:news_app/models/category_model.dart';
import 'package:news_app/views/article_view.dart';
import 'package:news_app/views/category_news.dart';

class MyHomeScreeen extends StatefulWidget {
  @override
  _MyHomeScreeenState createState() => _MyHomeScreeenState();
}

class _MyHomeScreeenState extends State<MyHomeScreeen> {
  bool _isLoading = false;

  List<CategoryModel> categories = new List<CategoryModel>();

  List<ArticleModel> news = new List<ArticleModel>();

  @override
  void initState() {
    super.initState();

    categories = getCategories();

    this.getArticles();
  }

  /// fetch api data from news api url
  getArticles() async {
    
    setState(() {
      _isLoading = true;
    });

    ApiArticleList apiArticleClass = ApiArticleList();

    await apiArticleClass.getNews();

    news = apiArticleClass.news;

    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.white,
        elevation: 0.0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Flutter',
              style: TextStyle(color: Colors.black87),
            ),
            Text('News',
                style: TextStyle(
                    color: Colors.blue,
                    letterSpacing: 1.0,
                    fontWeight: FontWeight.w600))
          ],
        ),
      ),
      body: _isLoading
          ? Center(
              child: Container(
                child: CircularProgressIndicator(),
              ),
            )
          : SingleChildScrollView(
              child: Container(
                child: Column(
                  children: [
                    /* 1 Widget of Category Box */
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 5.0),
                      height: 70.0,
                      child: ListView.builder(
                          itemCount: categories.length,
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            return CategoryBox(
                              imageUrl: categories[index].imageUrl,
                              categoryName: categories[index].categoryName,
                            );
                          }),
                    ),

                    /* 2 widget Blogs data from api */
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 7.0),
                      child: ListView.builder(
                          itemCount: news.length,
                          shrinkWrap: true,
                          physics: ClampingScrollPhysics(),
                          itemBuilder: (context, index) {
                            return News(
                              title: news[index].title,
                              description: news[index].description,
                              imageUrl: news[index].urlImage,
                              url: news[index].url,
                              publishedAt: news[index].publishedAt.toString(),
                            );
                          }),
                    )
                  ],
                ),
              ),
            ),
    );
  }
}

/* 1 Widgets Category Box */
class CategoryBox extends StatelessWidget {
  final imageUrl, categoryName;

  CategoryBox({this.imageUrl, this.categoryName});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) =>
                  CategoryNews(category: categoryName.toLowerCase()))),
      child: Container(
        margin: EdgeInsets.only(right: 10),
        child: Stack(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: Image.network(
                imageUrl,
                width: 120,
                height: 60,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              alignment: Alignment.center,
              width: 120,
              height: 60,
              decoration: BoxDecoration(
                  color: Colors.black26,
                  borderRadius: BorderRadius.circular(10.0)),
              child: Text(
                categoryName,
                style: TextStyle(
                  color: Colors.white,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

/* 2 widgets*/
class News extends StatelessWidget {
  final String imageUrl, title, description, url, publishedAt;

  News(
      {@required this.imageUrl,
      @required this.title,
      @required this.description,
      @required this.url,
      @required this.publishedAt});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ArticlesView(
                    linkURL: url,
                  ))),
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 6.0),
        child: Column(
          children: [
            ClipRRect(
                borderRadius: BorderRadius.circular(13.0),
                child: Image.network(imageUrl)),
            SizedBox(height: 20.0),
            Text(
              title,
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.w700),
            ),
            SizedBox(height: 20.0),
            Text(
              description,
              style: TextStyle(color: Colors.grey, fontSize: 17.0),
            ),
            SizedBox(height: 10.0),
            Text(
              publishedAt,
              style: TextStyle(color: Colors.grey, fontSize: 11.0),
            ),
          ],
        ),
      ),
    );
  }
}
