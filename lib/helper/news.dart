import 'dart:convert';
import 'package:news_app/models/article.dart';
import 'package:http/http.dart' as http;

/* article news */
class ApiArticleList {

  List<ArticleModel> news = [];

  Future<void> getNews() async {
    String apiURL =
        'http://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=95b56170548d47f998946d03303c6cd5';

    var response = await http.get(apiURL);

    var jsonData = jsonDecode(response.body);

    if (jsonData['status'] == "ok") {
      jsonData["articles"].forEach((element) {
        if (element["urlToImage"] != null && element['description'] != null) {

          new DateTime.now();
          new DateTime.utc(1989, 11, 9);
          var moonLanding = DateTime.parse(element['publishedAt']);

          ArticleModel articleModel = new ArticleModel(
              title: element['title'],
              author: element['author'],
              description: element['description'],
              url: element['url'],
              urlImage: element['urlToImage'],
              publishedAt: moonLanding,
              content: element['content']);




          // add news in arraymodel list
          news.add(articleModel);
        }
      });
    }
  }
}








