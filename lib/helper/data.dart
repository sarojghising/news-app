import 'package:news_app/models/category_model.dart';

List<CategoryModel> getCategories() {

  List<CategoryModel> category = new List<CategoryModel>();

  //1
  CategoryModel categoryModel = new CategoryModel();
  categoryModel.categoryName = "Business";
  categoryModel.imageUrl = "https://images.unsplash.com/photo-1593642634367-d91a135587b5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80";
  category.add(categoryModel);

 //2
 CategoryModel categoryModel1 = new CategoryModel();
  categoryModel1.categoryName = "Entertainment";
  categoryModel1.imageUrl = "https://images.unsplash.com/photo-1486406146926-c627a92ad1ab?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80";
  category.add(categoryModel1);


//1
  CategoryModel categoryModel3 = new CategoryModel();
  categoryModel3.categoryName = "General";
  categoryModel3.imageUrl = "https://images.unsplash.com/photo-1593642634367-d91a135587b5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80";
  category.add(categoryModel3);

 //2
 CategoryModel categoryModel4 = new CategoryModel();
  categoryModel4.categoryName = "Health";
  categoryModel4.imageUrl = "https://images.unsplash.com/photo-1486406146926-c627a92ad1ab?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80";
  category.add(categoryModel4);

//1
  CategoryModel  categoryModel2 = new CategoryModel();
  categoryModel2.categoryName = "Science";
  categoryModel2.imageUrl = "https://images.unsplash.com/photo-1593642634367-d91a135587b5?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80";
  category.add(categoryModel2);

 //2
 CategoryModel categoryModel5 = new CategoryModel();
  categoryModel5.categoryName = "Sports";
  categoryModel5.imageUrl = "https://images.unsplash.com/photo-1486406146926-c627a92ad1ab?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80";
  category.add(categoryModel5);


 //2
 CategoryModel categoryModel6 = new CategoryModel();
  categoryModel6.categoryName = "Technology";
  categoryModel6.imageUrl = "https://images.unsplash.com/photo-1486406146926-c627a92ad1ab?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1050&q=80";
  category.add(categoryModel6);


  return category;


}