/* category */
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:news_app/models/category_model.dart';

class CategoryArticles {

   List<CategoryModel> categories = [];

  Future<void>  getCategoryArticles(String category) async {
    String apiURL =
        'https://newsapi.org/v2/top-headlines?category=$category&apiKey=95b56170548d47f998946d03303c6cd5';

    var response = await http.get(apiURL);

    var jsonData = jsonDecode(response.body);

    if (jsonData['status'] == "ok") {

      jsonData["articles"].forEach((element) {

        if (element["urlToImage"] != null && element['author'] != null) {

          CategoryModel categoryModel = new CategoryModel(

            categoryName: element['author'],
            imageUrl : element['urlToImage']
          
          );
          
          // add news in arraymodel list
          categories.add(categoryModel);
        }
      });
    }
  }
 
}
